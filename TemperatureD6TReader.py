# coding: utf-8
# Sample that outputs the value acquired by D6T.
# Please execute the following command before use "pigpio"
#  $ sudo pigpio

import D6T32L_Driver as d6t32l
import pigpio
import time

d6t = d6t32l.GroveD6t32l()

myValue = [0 for i in range(2)]

#while True:
def GetValue(myValue):
    try:
        tpn, tptat = d6t.readData()
        #if tpn == None:
        #    continue
        
        myValue[0] = max(tpn)
        myValue[1] = min(tpn)
        #print(tpn,"PTAT : %.1f" %tptat)
        #print(max(tpn))
        #print(min(tpn))
        
        #print(TR2_Value)
        #time.sleep(0.1)
        tpn.clear()
    except IOError:
        print("IOError")
