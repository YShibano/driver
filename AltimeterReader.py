#codeing: UTF-8
#-------------------------------------------------------------------------------------------
# LightWare RaspberryPi I2C connection sample
# https:#lightware.co.za
#-------------------------------------------------------------------------------------------
# Compatible with the following devices:
# - SF02
# - SF10
# - SF11
# - LW20/SF20
#-------------------------------------------------------------------------------------------

import time
import debug
import math
import csv
if debug.ADEBUG: 
  import random
else:
  import smbus

from multiprocessing import Value, Array

#CONST
ADDRS = [0x61, 0x62, 0x63]
WIDTH = 49.4  # [cm] Distance between Sensor 1 and 2
LENGTH = 192.05  # [cm] Distance between calculated x1(1-2) and Sensor 3
CALIB_OFFSET = [49, 46, 48] # [cm] calibration(offset) 0:0x61, 1:0x62, 2:0x63

#global var
new_write_flg = 1
buf = [0 for i in range(len(ADDRS))]
myValue = [0 for i in range(8)]

# Initialize the I2C Bus with I2C File 1. (Older Pis need File 0)
if not debug.ADEBUG :
  i2c = smbus.SMBus(4)

#Read a distance value from each sensors 
def DistanceReader(dist_data):
  if debug.ADEBUG :
    for index, addr in enumerate(ADDRS):
      dist_data[index] = random.randint(0,100)
      print(index)
      print(dist_data[index])
  else: 
    #for文を使ったがうまくいかず(残骸)
    # dist_data[index] = i2c.read_word_data(addr, 0)  
    # dist_data[index] = ((dist_data[index] >> 8) | (dist_data[index] << 8)) & 0xFFFF + CALIB_OFFSET[index]

    #for文を使わず記述(過去に数値が怪しかったため)
    #reading1 = i2c.read_word_data(0x61, 0)
    #dist_data[0]=((reading1 >> 8) | (reading1 << 8)) &   0xFFFF
    #reading2 = i2c.read_word_data(0x62, 0)
    #dist_data[1]=((reading2 >> 8) | (reading2 << 8)) &   0xFFFF
    #reading3 = i2c.read_word_data(0x63, 0)
    #dist_data[2]=((reading3 >> 8) | (reading3 << 8)) &   0xFFFF
    
    #Yoppy ver.
    for i in range(len(ADDRS)):
        try:
            buf[i] = i2c.read_word_data(ADDRS[i], 0)
            dist_data[i] = (((buf[i] >> 8) | (buf[i] << 8)) & 0xFFFF ) - CALIB_OFFSET[i]
        except IOError:
            dist_data[i] = -9999
        
    #Offsetを反映
    #for i, offset in enumerate(CALIB_OFFSET):
      #dist_data[i]-=offset
      
#Measure Altitude
  #logic description
  #***
  #  Calc height from 3 sensors:
  #    two rear sensors dist and front sensor dist.
  #        
def MeasureAltitude(dist1,dist2,dist3,myValue):
        
#1.Calculate Rear altitude from 2 RearSesors.
#1-1.the difference between sensor1-dist and sensor2-dist by role-angle(unknown)
  ldiff_a = math.fabs(dist1 - dist2)
#1-2.calucurates the roll-angle of the aircraft
#    from the rear-sensor difference and WIDTH
  role_deg = math.degrees(math.atan(ldiff_a / WIDTH))
#1-3.judge the short side(left or right in rearside)
  if dist1 < dist2:
    tmp_shortside1 = dist1
  else:
    tmp_shortside1 = dist2
#1-4.Rear altitude
  alt1 = ( tmp_shortside1 + (ldiff_a/2) ) * math.cos(math.radians(role_deg))

#2.Calculate Rear altitude from 2 RearSesors.
#2-1.Adapt the roll angke to the front sensor. 
  dist3_tmp = dist3 * math.cos(math.radians(role_deg))
#2-2.the difference between Front-altitude and Rear-altitude by pitch-angle(unknown)
  ldiff_b = math.fabs(dist3_tmp - alt1)#already adapted role-angle 
#2-2.calucurates the pitch-angle of the aircraft
#    from the front&rear-sensors difference and aircraft length
  pitch_deg = math.degrees(math.atan(ldiff_b / LENGTH))
#2-3.judge the short side(front or rear in frontside)[already adapted role-angle ]
  if (alt1 < dist3_tmp) :
    tmp_shortside2 = alt1
  else :
    tmp_shortside2 = dist3_tmp
  
#2-4.front altitude
  alt2 = dist3_tmp * math.cos(math.radians(pitch_deg))

#3.center altitude
  alt3 = (tmp_shortside2 + (ldiff_b/2)) * math.cos(math.radians(pitch_deg))

#m換算(x0.01)
  myValue[0]= dist1 #0x61 distance
  myValue[1]= dist2 #0x62 distance
  myValue[2]= dist3 #0x63 distance
  myValue[3]=round(alt1*0.01,2)  #Rearside altitude
  myValue[4]=round(alt2*0.01,2)  #Frontside altitude
  myValue[5]=round(alt3*0.01,2)  #Center altitude
  myValue[6]=round(role_deg,2)
  myValue[7]=round(pitch_deg,2)
  

  if debug.ADEBUG:
    f = open("/home/pi/Desktop/010_code_Lite2/driver/Indicator/Alt-test.csv", "w+", newline='')
    try:
      global new_write_flg
      writer = csv.writer(f)
      if new_write_flg:
        if 0:
          writer.writerow(['dist1','dist2',
                            'dist3','AltRear',
                            'AltFront','AltCenter',
                            'role','pitch'])
        new_write_flg = 0
      #for data_row in myValue:
      writer.writerow([int(myValue[0]), int(myValue[1]),
                        int(myValue[2]), int(myValue[3]),
                        int(myValue[4]), int(myValue[5]),
                        int(myValue[6]), int(myValue[7])])
      #new_write_flg = 0

    finally:
      f.close()

if debug.ADEBUG:
  while 1:    
    #def StartAltimeterReader(myValue):
    myValue=Array('d',8)
    dist_data=[0,0,0] #1:0x61, 2:0x62, 3:0x63
    #altitude=[]  #1:Rearside, 2:Frontside, 3:Center
    DistanceReader(dist_data)
    MeasureAltitude(dist_data[0], dist_data[1], dist_data[2], myValue)
    for value in myValue:
      print(value)
    time.sleep(0.05)#秒指定

#while True:
def GetValues(myValue):
    dist_data=[0,0,0] #1:0x61, 2:0x62, 3:0x63
    #altitude=[]  #1:Rearside, 2:Frontside, 3:Center
    DistanceReader(dist_data)
    
    #if sensor error detect, its value will be set -9999.
    ErrorFlag = False
    for i in range(len(dist_data)):
        if dist_data[i] == -9999:
            ErrorFlag = True
        else:
            dist_data[i] = round(dist_data[i]*0.01, 2)
            ErrorFlog = False
    
    if ErrorFlag == False:
        MeasureAltitude(dist_data[0], dist_data[1], dist_data[2], myValue)
    else:
        for i in range(len(dist_data)):
            myValue[i] = dist_data[i]
        myValue[3] = -9999
        myValue[4] = -9999
        myValue[5] = -9999
        myValue[6] = -9999
        myValue[7] = -9999
        
    #print(myValue)
    #time.sleep(0.1)
    
    if debug.ADEBUG:
        for value in myValue:
            print(value)
  #time.sleep(0.05)#秒指定

